let body = document.querySelector('body');
let gameboardDiv = document.getElementById('gameboard');
let startButton = document.getElementById('start');
let charDiv = document.getElementById('characters');
let charImgDiv = document.getElementById('char-faces');
let scoreboardDiv = document.getElementById('scoreboard');
let asideDiv = document.querySelector('aside');
let turnDiv = document.getElementById('turn');

let redWinCount = 0;
let blackWinCount = 0;
let placedPieces = 0;

let selectedCount = 0;   //selected characters on character select screen

let columnCount = 7;
let rowCount = 6;
let connectCount = 4;

let turn = 'red'; 
let gameOver = false;

let player1;
let player2;

//Sound
let bgMusic;
let gruntSound = new sound('./audio/grunt.ogg');
let winSound = new sound('./audio/proud.ogg');

//create char select on load
createCharSelect()

//for safari compatability
//TODO:get IE working
function getBrowser() {
    let nAgt = navigator.userAgent;
    let browserName  = navigator.appName;

    let obscureCase = (nameOffset=nAgt.lastIndexOf(' ')+1) < 
    (verOffset=nAgt.lastIndexOf('/'))
    
    if ((verOffset=nAgt.indexOf("Opera"))!=-1) {
        browserName = "Opera";
    } else if ((verOffset=nAgt.indexOf("MSIE"))!=-1) {
        browserName = "Microsoft Internet Explorer";
    } else if ((verOffset=nAgt.indexOf("Chrome"))!=-1) {
        browserName = "Chrome";
    } else if ((verOffset=nAgt.indexOf("Safari"))!=-1) {
        browserName = "Safari";
    } else if ((verOffset=nAgt.indexOf("Firefox"))!=-1) {
        browserName = "Firefox";
    } else if (obscureCase) {
        browserName = nAgt.substring(nameOffset,verOffset);
        if (browserName.toLowerCase()==browserName.toUpperCase()) {
            browserName = navigator.appName;
        }
    }
    return browserName
}

// -----------Character Select---------
// create an img element for char select screen
function createImgElement(i, charArr) {
    let curImgEl = document.createElement('img')
    curImgEl.src = "./img/"+charArr[i].toLowerCase()+".png"
    curImgEl.id = charArr[i]
    curImgEl.addEventListener('click', function charClickHandler() {
        if(selectedCount<2) {
            if (turn == 'red') {
                player1 = charArr[i]
                curImgEl.style.outline = "4px solid red"
                turn = 'black'
                selectedCount++
            } else if (turn == 'black') {
                if(charArr[i]!=player1){
                    player2 = charArr[i]
                    curImgEl.style.outline = "4px solid green"
                    turn = player1
                    selectedCount++
                } else {
                    alert('character already chosen, choose a different character')
                }
            }
        } else {
            alert('players already selected, press the button to start already.')
            return
        }
        if(selectedCount==2){
            let startButton = createStartButton()
            charDiv.append(startButton)
        }
    })
    return curImgEl
}

//create char select div from charArr
function createCharSelect() {
    let charArr = ['Chok', 'Jake', 'Brule', 'Erika', 'DMG', 'Loops', 'Chippy', 'Alyssa', 'Matt', 
    'Kavitha', 'Georgie', 'Bob']
    for (let i = 0; i < charArr.length; i++) {
        let curImgEl = createImgElement(i, charArr)
        charImgDiv.appendChild(curImgEl)
    }
}

//create start button for character select screen
function createStartButton() {
    let startButton = document.createElement('button')
    startButton.classList.add('btn', 'is-primary')
    startButton.id = 'start'
    let startText = document.createTextNode("Let's GO")
    startButton.appendChild(startText)
    startButton.onclick = function start() {
        startButton.outerHTML = ''
        charDiv.style.display = 'none'
        buildBoard(gameboardDiv)
        createTurnDiv(turn)
        updateScoreBoard(redWinCount,blackWinCount)
        bgMusic = new sound('./audio/abed.mp3');
        bgMusic.sound.controls = true
        bgMusic.sound.style.display = 'block'
        bgMusic.sound.loop = true
        asideDiv.style.border = 1 +'px solid'
    }
    return startButton
}
// -----------------------------------

//create i columns w id coli
function createCol(i) {
    let newCol = document.createElement('div')
    newCol.classList.add('col')
    newCol.id = "col"+i
    newCol.addEventListener('click', clickHandler)
    return newCol
}

//create a cell w id celli-j
function createCell(i,j) {
    let newCell = document.createElement('div')
    newCell.classList.add('cell')
    newCell.id = "cell" + i + '-' + j
    return newCell
}

//builds the game board w/ create cell and create col 
function buildBoard(div) {
    for (let i = 0; i < columnCount; i++) {
        let newCol = createCol(i)
        for (let j = 0; j < rowCount; j++) {
            let newCell = createCell(i,j)
            newCol.appendChild(newCell)
        }
        div.appendChild(newCol)
    }
}

//create a piece based on turn
function createPiece() {
    let newPiece = document.createElement('div')
    if (turn==player1) {
        newPiece.classList.add('piece-red') 
        newPiece.style.backgroundImage = "url(./img/"+player1.toLowerCase()+".png)"
    } else if (turn==player2) {
        newPiece.classList.add('piece-black')
        newPiece.style.backgroundImage = "url(./img/"+player2.toLowerCase()+".png)"
    }
    return newPiece
}

//places a piece in current column
function placePiece(curColCells,curTar) {
    let browserName = getBrowser()
    //if column full / top cell occupied
    if (curColCells[1].childElementCount!==0){
        curTar.removeEventListener('click',clickHandler)
    } 
    
    newPiece = createPiece()
    for (let i = 0; i <= curColCells.length-1; i++) {
        let curCell = curColCells[i]
        let cellBelow = curColCells[i+1]
        //if at bottom place piece
        if (i == curColCells.length-1) {
            if (browserName!='Safari' && browserName!='Microsoft Internet Explorer'){
                animatePiece(newPiece,i)
            }
            curCell.appendChild(newPiece)
            placedPieces++
            break
        //else if cell below contains piece, place piece here
        } else if(cellBelow.childElementCount!==0){
            if (browserName!='Safari' && browserName!='Microsoft Internet Explorer'){
                animatePiece(newPiece,i)
            }
            curCell.appendChild(newPiece)
            placedPieces++
            break
        }
    }
    gruntSound.play()
    //update turn
    if (turn==player1) { 
        turn = player2
    } else if (turn==player2) {
        turn = player1
    }
}

//animates falling piece
function animatePiece(newPiece,row) {
    //rotation animation
    let pieceRotation = [
        { transform: 'rotate(0)'}, 
        { transform: 'rotate(360deg)'}
    ]
    
    let randDuration = Math.floor(Math.random() * 7) + 1

    let animationOptions = {
        duration: randDuration*1000,
        iterations: Infinity,
        direction: 'alternate'
    }
    newPiece.animate(pieceRotation,animationOptions)
    
    //falling animation
    newPiece.style.top = 0
    let pos = 0;
    let acc = 0.1
    let id = setInterval(frame,1);
    let yOffset = 0
    function frame() {
        if (pos >= (row*82)+yOffset) {
            clearInterval(id)
        } else {
            pos+=0.1+acc
            acc+=0.01
            newPiece.style.top = pos + 'px';
        }
    }
}

//----------------------------------START-WIN CHECKS
//checks board for vertical win
function vertWinCheck(board) {
    let checkEdge = connectCount-1
    for(let i = 0; i < board.length; i++) {
        let colCells = board[i].children
        for(let j = 0; j < colCells.length-checkEdge; j++) {
            let curColCell = colCells[j]
            let checkArray = []
            checkArray.push(curColCell)
            for (let k = 1; k < connectCount; k++) {
                checkArray.push(colCells[j+k])
            }
            checkForWin(checkArray)
        }
    }
}

//checks board for horizontal win 
function horizWinCheck(board) {
    let checkEdge = connectCount-1
    for(let i = 0; i < board.length-checkEdge; i++) {
        let colCells = board[i].children    
        for(let j = 0; j < colCells.length; j++) {
            let curColCell = colCells[j]
            let checkArray = []
            checkArray.push(curColCell)
            for (let k = 1; k < connectCount; k++){
                checkArray.push(board[i+k].children[j])
            }
            checkForWin(checkArray)
        }
    }
}

//checks board for down-right win 
function downRightWinCheck(board) {
    let checkEdge = connectCount-1
    for(let i = 0; i < board.length-checkEdge; i++) {
        let colCells = board[i].children    
        for(let j = 0; j < colCells.length-checkEdge; j++) {
            let curColCell = colCells[j]
            let checkArray = []
            checkArray.push(curColCell)
            for (let k = 1; k < connectCount; k++) {
                checkArray.push(board[i+k].children[j+k])
            }
            checkForWin(checkArray)
        }
    }
}

//checks board for up-right win 
function upRightWinCheck(board) {
    let checkEdge = connectCount-1
    for(let i = 0; i < board.length-checkEdge; i++) {
        let colCells = board[i].children  
        for(let j = checkEdge; j < colCells.length; j++) {
            let curColCell = colCells[j]
            let checkArray = []
            checkArray.push(curColCell)
            for (let k = 1; k < connectCount; k++) {
                checkArray.push(board[i+k].children[j-k])
            }
            checkForWin(checkArray)
        }
    }
}
//-----------------------------------END-WIN CHECKS

//check if checkArray is a winner
function checkForWin(checkArray) {
    let redPieceCount = 0
    let blackPieceCount = 0
    for (let k = 0; k < checkArray.length; k++) {
        if (checkArray[k].firstChild) {
            if (checkArray[k].firstChild.classList.contains('piece-red')) {
                redPieceCount++ 
                checkArray[k] = checkArray[k].firstChild
            } else if (checkArray[k].firstChild.classList.contains('piece-black')) {
                blackPieceCount++
                checkArray[k] = checkArray[k].firstChild
            } else {
                break
            }
        } else {
            break
        }
    }
    
    if (redPieceCount==connectCount) {
        redWinCount++
        winner(player1,checkArray)
        return
    } else if (blackPieceCount==connectCount) {
        blackWinCount++
        winner(player2,checkArray)
        return
    }
}

//create/update turn div
function createTurnDiv(turn) {
    let turnText = document.createTextNode(turn + "'s turn")
    turnDiv.innerHTML = ''
    turnDiv.appendChild(turnText)
}

//updates scoreboard
function updateScoreBoard(redWinCount,blackWinCount) {
    scoreboardDiv.innerHTML = ''
    
    let playersDiv = document.createElement('div')
    playersDiv.id = 'players'
    playersDiv.style.textDecoration = 'underline'
    let scoresDiv = document.createElement('div')

    let playersText = document.createTextNode(player1 + ' vs. ' + player2)
    let scoresText = document.createTextNode(redWinCount + ' to ' + blackWinCount)
    
    playersDiv.appendChild(playersText)
    scoresDiv.appendChild(scoresText)
    scoreboardDiv.appendChild(playersDiv)
    scoreboardDiv.appendChild(scoresDiv)
    return scoreboardDiv
}

//creates and returns win div
function createWinDiv(color) {
    let winDiv = document.getElementById('winner')
    winDiv.innerHTML = ''
    let winText = document.createTextNode('A winner is ' + color)
    winDiv.appendChild(winText)
    return winDiv
}

//creates and returns reset button 
function createResetButton(gameboardDiv,winDiv) {
    let resetButton = document.createElement('button')
    resetButton.type = 'button'
    resetButton.classList.add('btn', 'is-warning')
    resetButton.id = 'reset-button'
    resetButton.innerHTML = 'Reset Board'
    resetButton.style.marginTop = 10 + 'px'
    resetButton.onclick = function reset() {
        gameboardDiv.innerHTML = ''
        if (winDiv) winDiv.innerHTML = ''
        buildBoard(gameboardDiv)
        gameOver = false
        placedPieces = 0
    }
    return resetButton
}

//applies win condition
function winner(color,checkArray) {
    for (let i = 0; i < checkArray.length; i++) {
        checkArray[i].classList.add('winnerCell')
    }
    
    let winDiv = createWinDiv(color)
    let resetButton = createResetButton(gameboardDiv,winDiv)
    let scoreboardDiv = updateScoreBoard(redWinCount,blackWinCount)
    winDiv.appendChild(resetButton)
    asideDiv.appendChild(scoreboardDiv)
    asideDiv.appendChild(winDiv)
    winSound.play()
    gameOver = true

}

//tie condition
function tie() {
    gameOver = true
    redWinCount+=0.5
    blackWinCount+=0.5
    let winDiv = createWinDiv('no one')
    let resetButton = createResetButton(gameboardDiv,winDiv)
    let scoreboardDiv = updateScoreBoard(redWinCount,blackWinCount)
    winDiv.appendChild(resetButton)
    asideDiv.appendChild(scoreboardDiv)
    asideDiv.appendChild(winDiv)
}

//handles clicks
function clickHandler(event) {
    if(gameOver==false){
        let curTar = event.currentTarget
        let curColCells = curTar.children
        let board = gameboardDiv.children
        placePiece(curColCells,curTar)
        vertWinCheck(board)
        horizWinCheck(board)
        downRightWinCheck(board)
        upRightWinCheck(board)
        createTurnDiv(turn)
        if(placedPieces==columnCount*rowCount) {
            tie()
        }
    } else {
        let curTar = event.currentTarget
        curTar.removeEventListener('click',clickHandler)
    }
}

// W3 sounds example
function sound(src) {
    this.sound = document.createElement("audio");
    this.sound.src = src;
    this.sound.setAttribute("preload", "auto");
    this.sound.style.display = "none";
    body.appendChild(this.sound);
    this.play = function(){
        this.sound.play();
    }
    this.stop = function(){
        this.sound.pause();
    }
}

//TODO: add delay to win check and outline application
//TODO: animate pieces falling out of board on reset
//TODO: randomize piece backgrounds on board build
//TODO: add piece hover
//TODO: update directory structure
//TODO: refactor into classes